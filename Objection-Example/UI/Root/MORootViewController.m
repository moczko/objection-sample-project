//
//  Created by Maciej Oczko on 10/14/13.
//


#import "MORootViewController.h"
#import "Objection.h"
#import "MORootView.h"
#import "MOModalViewController.h"
#import "MOAccountManager.h"
#import "MOActionHandler.h"
#import "MOCoreDataManager.h"
#import "MOApiClient.h"
#import "MOKeyChainObjectionProvider.h"
#import "MOKeyChainAccessor.h"
#import "MOUserManager.h"

@implementation MORootViewController {

}

objection_initializer_sel(@selector(initWithNibName:bundle:))

- (void)loadView {
    MORootView *view = [[JSObjection defaultInjector] getObject:[MORootView class]];
    view.delegate = self;
    self.view = view;
}

#pragma mark -

- (void)openModalView {
    JSObjectionInjector *injector = [JSObjection defaultInjector];

    MOAccountManager *accountManager = injector[[MOAccountManager class]];
    MOModalViewController *controller = [injector getObject:[MOModalViewController class] argumentList:@[accountManager]];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark -

- (void)doNothing {
    JSObjectionModule *module = [[JSObjectionModule alloc] init];
    [module registerEagerSingleton:[MOCoreDataManager class]];
    [module bindBlock:^id(JSObjectionInjector *context) {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }         toClass:[UIView class]];
    [module bind:[[MOActionHandler alloc] initWithName:@"NeverChangingName"] toClass:[MOActionHandler class]];
    [module bindClass:[MOApiClient class] toProtocol:@protocol(MOApiClientProtocol)];
    [module bindProvider:[[MOKeyChainObjectionProvider alloc] init] toClass:[MOKeyChainAccessor class]];
    [module bindClass:[MOAccountManager class] inScope:JSObjectionScopeSingleton];
    [module bindClass:[MOUserManager class] inScope:JSObjectionScopeSingleton];

    JSObjectionInjector *injector = [JSObjection createInjector:module];
    MOAccountManager *accountManager = injector[[MOAccountManager class]];
    [accountManager hash];
}

@end
