//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>

@protocol MOModalViewDelegate;


@interface MOModalView : UIView
@property(nonatomic, weak) id <MOModalViewDelegate> delegate;
@end
