//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>

@protocol MOModalViewDelegate <NSObject>
- (void)hideModalView;
@end
