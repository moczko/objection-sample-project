//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Objection/Objection.h>
#import "MOModalViewController.h"
#import "MOModalView.h"
#import "MOAccountManager.h"


@implementation MOModalViewController {

}

objection_initializer_sel(@selector(initWithAccountManager:))

- (instancetype)initWithAccountManager:(MOAccountManager *)accountManager {
    self = [super init];
    if (self) {
        _accountManager = accountManager;
    }

    return self;
}

- (void)loadView {
    MOModalView *view = [JSObjection defaultInjector][[MOModalView class]];
    view.delegate = self;
    self.view = view;
}

#pragma mark -

- (void)hideModalView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
