//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>
#import "MOModalViewDelegate.h"

@class MOAccountManager;

@interface MOModalViewController : UIViewController <MOModalViewDelegate>
@property(nonatomic, readonly) MOAccountManager *accountManager;
- (instancetype)initWithAccountManager:(MOAccountManager *)accountManager;

@end
