//
//  main.m
//  Objection-Example
//
//  Created by Maciej Oczko on 10/14/13.
//  Copyright (c) 2013 Polidea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MOAppDelegate class]));
    }
}
