//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MOUserManager.h"
#import "MOCoreDataManager.h"
#import "MOKeyChainAccessor.h"
#import "Objection.h"
#import "Defines.h"


@interface MOUserManager ()
@property(nonatomic, readonly) MOCoreDataManager *coreDataManager;
@property(nonatomic, readonly) MOKeyChainAccessor *keyChainAccessor;
@end

@implementation MOUserManager {

}

objection_requires_sel(@selector(coreDataManager), @selector(keyChainAccessor))

- (void)awakeFromObjection {
    [super awakeFromObjection];

    assert(_coreDataManager != nil);
    assert(_keyChainAccessor != nil);
    HELP_LOG
}

@end
