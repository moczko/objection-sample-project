//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Objection/NSObject+Objection.h>
#import "MOKeyChainAccessor.h"
#import "Defines.h"


@implementation MOKeyChainAccessor {

}

+ (id)defaultAccessor {
    return [[self alloc] init];
}

- (void)awakeFromObjection {
    [super awakeFromObjection];
    HELP_LOG
}

@end
