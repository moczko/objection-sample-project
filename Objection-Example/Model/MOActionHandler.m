//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Objection/NSObject+Objection.h>
#import "MOActionHandler.h"
#import "Defines.h"


@implementation MOActionHandler {

}

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }

    return self;
}

- (void)awakeFromObjection {
    [super awakeFromObjection];
    HELP_LOG
}

@end
