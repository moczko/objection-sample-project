//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>

@protocol MOApiClientProtocol <NSObject>
- (void)performRequest;
- (NSURLRequest *)buildRequest:(NSDictionary *)options;
@end
