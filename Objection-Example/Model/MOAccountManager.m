//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Objection/Objection.h>
#import "MOAccountManager.h"
#import "MOApiClientProtocol.h"
#import "MOUserManager.h"
#import "Defines.h"


@interface MOAccountManager ()
@property(nonatomic, readonly) id <MOApiClientProtocol> apiClient;
@property(nonatomic, readonly) MOUserManager *userManager;
@end

@implementation MOAccountManager {

}

objection_requires_sel(@selector(apiClient), @selector(userManager))

- (id)init {
    self = [super init];
    if (self) {
        assert(_apiClient == nil);
    }

    return self;
}

- (void)awakeFromObjection {
    [super awakeFromObjection];

    assert(_apiClient != nil);
    assert(_userManager != nil);

    HELP_LOG
}

@end
