//
//  Created by Maciej Oczko on 10/14/13.
//


#import <Foundation/Foundation.h>


@interface MOCoreDataManager : NSObject {
@protected
    NSManagedObjectContext *_mainContext;
    NSManagedObjectContext *_backgroundContext;

    NSManagedObjectModel *_managedObjectModel;
    NSPersistentStoreCoordinator *_persistentStoreCoordinator;
}
@property(nonatomic, readonly) NSManagedObjectContext *mainContext;
@property(nonatomic, readonly) NSManagedObjectContext *backgroundContext;
@end
