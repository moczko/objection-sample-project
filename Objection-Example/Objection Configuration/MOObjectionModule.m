//
//  Created by Maciej Oczko on 10/14/13.
//


#import "MOObjectionModule.h"
#import "MOCoreDataManager.h"
#import "MOActionHandler.h"
#import "MOApiClient.h"
#import "MOAccountManager.h"
#import "MOUserManager.h"
#import "MOKeyChainAccessor.h"
#import "MOKeyChainObjectionProvider.h"


@implementation MOObjectionModule {

}

- (void)configure {
    [super configure];

    [self registerEagerSingleton:[MOCoreDataManager class]];

    [self bindBlock:^id(JSObjectionInjector *context) {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }       toClass:[UIView class]];

    [self bind:[[MOActionHandler alloc] initWithName:@"NeverChangingName"] toClass:[MOActionHandler class]];

    [self bindClass:[MOApiClient class] toProtocol:@protocol(MOApiClientProtocol)];

    [self bindProvider:[[MOKeyChainObjectionProvider alloc] init] toClass:[MOKeyChainAccessor class]];

    [self bindClass:[MOAccountManager class] inScope:JSObjectionScopeSingleton];
    [self bindClass:[MOUserManager class] inScope:JSObjectionScopeSingleton];
}

@end
