//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>
#import <Objection/JSObjectionModule.h>


@interface MOKeyChainObjectionProvider : NSObject <JSObjectionProvider>
@end
