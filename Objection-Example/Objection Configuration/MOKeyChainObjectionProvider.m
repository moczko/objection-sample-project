//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Objection/JSObjectionInjector.h>
#import "MOKeyChainObjectionProvider.h"
#import "MOKeyChainAccessor.h"


@implementation MOKeyChainObjectionProvider {

}

- (id)provide:(JSObjectionInjector *)context arguments:(NSArray *)arguments {
    return [MOKeyChainAccessor defaultAccessor];
}

@end
